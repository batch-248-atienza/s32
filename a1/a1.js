let http = require('http');
let port = 4000;

http.createServer(function(request, response){
	if(request.url == '/' && request.method == 'GET'){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Welcome to Booking System');
	}else if(request.url == '/profile' && request.method == 'GET'){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Welcome to your profile!');
	}else if(request.url == '/courses' && request.method == 'GET'){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Here\'s our courses availabe');
	}else if(request.url == '/addcourse' && request.method == 'POST'){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Add a course to our resources');
	}else if(request.url == '/updatecourse' && request.method == 'PUT'){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Update a course to our resources');
	}else if(request.url == '/archivecourses' && request.method == 'DELETE'){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Archive a course to our resources');
	}
}).listen(port);

console.log(`Server running at localhost:${port}`);