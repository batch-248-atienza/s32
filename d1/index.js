let http = require('http');

http.createServer(function(request, response){

	console.log(request.url);
	console.log(request.method);

	/*
		HTTP Requests are differentiated not only via their endpoints but also with their methods

		HTTP Methods simply tells the server what action it must take or what kind of response is needed for the request

		With an HTTP Method we can actually create routes with the same endpoint but with different methods


	*/

	//url:localhost:4000/items
	//method: GET

	if(request.url == '/items' && request.method == 'GET'){

		//Requests the '/items' path and 'GETS' information
		response.writeHead(200,{'Content-Type':'text/plain'});
		//Ends the response process
		response.end('Data retrieved from the database!');

	}

	else if(request.url == '/items' && request.method == 'POST'){

		//Requests the '/items' path and 'SENDS' information
		response.writeHead(200,{'Content-Type':'text/plain'});
		//Ends the response process
		response.end('Data to be sent to the database!');

	}




}).listen(4000);

console.log('Server running at localhost:4000');